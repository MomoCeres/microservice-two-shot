from django.db import models
from django.urls import reverse

# Create your models here.
class LocationVO(models.Model):
    closet_name = models.CharField(max_length=200, unique=True)
    import_href = models.CharField(max_length=200, unique=True)
    # section_number = models.PositiveSmallIntegerField()
    # shelf_number = models.PositiveSmallIntegerField()

class Hats(models.Model):
    Fabric = models.CharField(max_length=100)
    Style = models.CharField(max_length=100)
    Color = models.CharField(max_length=100)
    Image = models.URLField(null=True)

    Location = models.ForeignKey(
        LocationVO,
        related_name="hats",
        on_delete=models.CASCADE,
        null=True,
    )

    def get_api_url(self):
        return reverse("api_show_hat", kwargs={"pk": self.id})