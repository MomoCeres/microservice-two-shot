from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from common.json import ModelEncoder
from .models import LocationVO, Hats


class LocationVOEncoder(ModelEncoder):
    model = LocationVO
    properties = ["closet_name", "import_href"]

class HatListEncoder(ModelEncoder):
    model = Hats
    properties = ["Fabric", "Style", "Color", "Location", "Image"]

    def get_extra_data(self, o):
        return {"Location": o.Location.closet_name}

class HatDetailEncoder(ModelEncoder):
    model = Hats
    properties = [
        "Fabric",
        "Style",
        "Color",
        "Image",
        "Location",
    ]
    encoders = {"Location": LocationVOEncoder()}

@require_http_methods(["GET", "POST"])
def api_lists_hats(request, location_vo_id=None):
    if request.method == "GET":
        if location_vo_id is not None:
            hats = Hats.objects.filter(location=location_vo_id)
        else:
            hats = Hats.objects.all()
        return JsonResponse(
            {"hats": hats},
            encoder=HatListEncoder
        )
    else:
        content = json.loads(request.body)
        try:
            location_href = content["Location"]
            location = LocationVO.objects.get(import_href=location_href)
            content["Location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location"},
                status=400,
            )
        hat = Hats.objects.create(**content)
        return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False,
        )

@require_http_methods(["DELETE", "GET"])
def api_show_hat(request, pk):
    if request.method == "GET":
        hat = Hats.objects.get(id=pk)
        return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        try:
            count, _ = Hats.objects.filter(id=pk).delete()
            return JsonResponse({"deleted": count > 0})
        except Hats.DoesNotExist:
            return JsonResponse[{"message": "Does not exist"}]

