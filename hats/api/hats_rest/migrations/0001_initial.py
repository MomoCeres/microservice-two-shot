# Generated by Django 4.0.3 on 2022-09-08 17:54

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='BinVO',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('wardrobe_name', models.CharField(max_length=200)),
                ('import_href', models.CharField(max_length=200, unique=True)),
            ],
        ),
        migrations.CreateModel(
            name='Hats',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('Fabric', models.CharField(max_length=100)),
                ('Style', models.CharField(max_length=100)),
                ('Color', models.CharField(max_length=100)),
                ('Image', models.URLField(null=True)),
                ('Bin', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='hats', to='hats_rest.binvo')),
            ],
        ),
    ]
