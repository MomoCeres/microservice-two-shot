import React from "react";
import { NavLink } from "react-router-dom";

class List_Hats extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            hats: []
        };
    }

async componentDidMount() {
    const url = ('http://localhost:8090/api/hats/');
    const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            console.log(data);
            this.setState({hats: data.hats})
        }
    }

    handleDelete = async (event) => {
        const href = event.target.value;
        console.log(href)
        const url = `http://localhost:8090${href}`;
        const fetchConfig = {
          method: "DELETE",
          headers: {
            'Content-Type': 'application/json'
          },
        };
        const response = await fetch(url, fetchConfig);
        if (response.ok) {
          this.setState({hats: this.state.hats.filter(hat => hat.href != href)})
        }
    }

    render () {
        return (
            <>
            <div className="hat list">
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th>Style</th>
                            <th>Fabric</th>
                            <th>Color</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.hats.map((Hat, index) => {
                            return (
                                <tr key={index}>
                                <td>{Hat.Style}</td>
                                <td>{Hat.Fabric}</td>
                                <td>{Hat.Color}</td>
                                <td><img className="img-thumbnail rounded img-responsive" height="100px" width="100px" src={Hat.Image} /></td>
                                <td>{Hat.Location}</td>
                                <td>
                                    <button onClick={this.handleDelete} value={Hat.href} 
                                    className="btn btn-outline-danger btn-sm">DELETE</button></td>
                                </tr>
                            );
                        })}
                    </tbody>
                </table>
            </div>
            <div>
                <NavLink to='/hats/new'>Create New Hat</NavLink>
            </div>
            </>
        );
    };
}

export default List_Hats
