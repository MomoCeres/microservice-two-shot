import React from 'react';

class NewHatForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            Fabric: '',
            Color: '',
            Style: '',
            Image: '',
            Location: '',
            locations: [],
        };
        this.handleFabricChange = this.handleFabricChange.bind(this);
        this.handleStyleChange = this.handleStyleChange.bind(this);
        this.handleColorChange = this.handleColorChange.bind(this);
        this.handleImageChange = this.handleImageChange.bind(this);
        this.handleLocationChange = this.handleLocationChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    async componentDidMount() {
        const url = 'http://localhost:8100/api/locations/';
        const response = await fetch(url)

        if (response.ok) {
            const data = await response.json();
            this.setState({locations: data.locations});
            console.log(data)
        }
    }

    handleSubmit = async (event) => {
        event.preventDefault();
        const data = {...this.state};
        delete data.locations;
        console.log(this.state)
        const hatsUrl = 'http://localhost:8090/api/hats/';
        const fetchOptions = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            },
        };
        const hatResponse = await fetch(hatsUrl, fetchOptions);
        if (hatResponse.ok) {
            const newHat = await hatResponse.json();
            const cleared = {
                Fabric: '',
                Color: '',
                Style: '',
                Image: '',
                Locations: '',
            }
            this.setState(cleared);
        }
    }
    handleColorChange(event) {
        const value = event.target.value;
        this.setState({Color: value})
    }
    handleFabricChange(event) {
        const value = event.target.value;
        this.setState({Fabric: value})
    }
    handleStyleChange(event) {
        const value = event.target.value;
        this.setState({Style: value})
    }
    handleImageChange(event) {
        const value = event.target.value;
        this.setState({Image: value})
    }
    handleLocationChange(event) {
        const value = event.target.value;
        this.setState({Location: value})
    }

render () {
    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Add a new hat</h1>
            <form onSubmit={this.handleSubmit} id="add-hat-form">
              <div className="form-floating mb-3">
                <input value={this.state.Style} onChange={this.handleStyleChange} placeholder="Style" required type="text" name="Style" id="Style" className="form-control" />
                <label htmlFor="Style">Style</label>
              </div>
              <div className="form-floating mb-3">
                <input value={this.state.Fabric} onChange={this.handleFabricChange} placeholder="Fabric" required type="text" name="Fabric" id="Fabric" className="form-control" />
                <label htmlFor="Fabric">Fabric</label>
              </div>
              <div className="form-floating mb-3">
                <input value={this.state.Color} onChange={this.handleColorChange} placeholder="Color" required type="text" name="Color" id="Color" className="form-control" />
                <label htmlFor="Color">Color</label>
              </div>
              <div className="form-floating mb-3">
                <input value={this.state.Image} onChange={this.handleImageChange} placeholder="Image" required type="url" name="Image" id="Image" className="form-control" />
                <label htmlFor="Image">Image</label>
              </div>
              <div className="mb-3">
                <select onChange={this.handleLocationChange} required name="Location" id="Location" className="form-select">
                  <option value="">Choose a closet</option>
                  {this.state.locations.map(location => {
                    return (
                      <option key={location.id} value={location.href}>
                        {location.closet_name}
                      </option>
                    );
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default NewHatForm;