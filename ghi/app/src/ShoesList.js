import React from 'react';
import { NavLink, Outlet, BrowserRouter, Routes, Route } from "react-router-dom";


class ListShoes extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      shoes: []
    };
  }

  async componentDidMount() {
    const url = 'http://localhost:8080/api/shoes/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      this.setState({ shoes: data.shoes });
    }
  }

  handleDelete = async (event) => {
    const id = event.target.value;
    const url = `http://localhost:8080/api/shoes/${id}`;
    const fetchConfig = {
      method: "delete",
      headers: {
        'Content-Type': 'application/json'
      },
    };
    const response = await fetch(url, fetchConfig);
    if (response.ok) {
      this.setState({
        shoes: this.state.shoes.filter(shoe => shoe.id != id)
      })
    }

  }


  render() {
    return (
      <>
        <div className="my-5 container">
          <div>
            <button type="button" className="btn btn-outline-info btn-sm">
              <NavLink to="/shoes/new" className="link-info"> Create New Shoe </NavLink>
            </button>
            <hr />
          </div>
          <table className="table table-info">
            <thead>
              <tr>
                <th>Manufacturer</th>
                <th>Bin</th>
                <th>Model Name</th>
                <th>Color</th>
                <th>Image</th>
                <th>Delete</th>
              </tr>
            </thead>
            <tbody>
              {this.state.shoes.map(shoe => {
                return (
                  <tr key={shoe.id}>
                    <td>{shoe.manufacturer}</td>
                    <td>{shoe.bin}</td>
                    <td>{shoe.model_name}</td>
                    <td>{shoe.color}</td>
                    <td><img className="img-thumbnail rounded img-responsive" width="80px" src={shoe.image} /></td>
                    <td>
                      <button onClick={this.handleDelete} value={shoe.id} className="btn btn-outline-danger btn-sm">DELETE</button>
                    </td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </div>
        <Outlet />
      </>
    );
  }
} 

export default ListShoes;